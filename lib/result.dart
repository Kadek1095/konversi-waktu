import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  var sisa;

  Result({
    @required this.totdet,
  });
  final int totdet;

  @override
  Widget build(BuildContext context) {
    int jam = (totdet / 3600).floor();
    int sisa = totdet % 3600;
    int menit = (sisa / 60).floor();
    int detik = sisa % 60;

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('RESULT'),
        ),
        body: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: new Text(
                  '$jam Jam : $menit Menit : $detik Detik',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w400,
                    color: Colors.green,
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
