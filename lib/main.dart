import 'package:flutter/material.dart';
import 'konversi.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.cyan[800],
        fontFamily: 'Georgia',
      ),
      home: InputKonversi(),
    ));
