import 'package:flutter/material.dart';

class Bio extends StatelessWidget {
  const Bio({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("PROFIL"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 20, bottom: 10),
                  child: Center(
                    child: CircleAvatar(
                        backgroundImage: AssetImage('images/INDRA.jpg'),
                        radius: 100),
                  )),
              Container(
                  child: Center(
                      child: Text('Kadek Indra Kusuma',
                          style: TextStyle(
                            fontSize: 20,
                          )))),
              Container(
                width: 500,
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.cyan[800]),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Text(
                    "Kadek Indra Kusuma adalah seorang mahasiswa di lingkungan Undikhsa dan sedang menempuh pendidikan di program studi Pendidikan Teknik Informatika. Ia berasal dari Desa Joanyar"),
              )
            ],
          ),
        ));
  }
}
