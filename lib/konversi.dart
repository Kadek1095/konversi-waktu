import 'package:flutter/material.dart';
import 'result.dart';
import 'bio.dart';

class InputKonversi extends StatefulWidget {
  @override
  _InputKonversiState createState() => _InputKonversiState();
}

class _InputKonversiState extends State<InputKonversi> {
  int totdet = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Konversi Waktu'),
      ),
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text(
                "KADEK INDRA KUSUMA",
                style: new TextStyle(fontWeight: FontWeight.bold),
              ),
              accountEmail: new Text("indra.kusuma.2@undiksha.ac.id"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('images/INDRA.jpg'),
              ),
              decoration: new BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Profil"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Bio(),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.info),
              title: Text("Tentang"),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Keluar"),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 40),
              alignment: Alignment(0.0, 0.0),
              padding: EdgeInsets.all(10),
              child: TextField(
                onChanged: (txt) {
                  setState(() {
                    totdet = int.parse(txt);
                  });
                },
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                ),
                decoration:
                    InputDecoration(filled: true, hintText: 'Total Detik'),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, right: 10, top: 10),
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Result(
                              totdet: totdet,
                            )),
                  );
                },
                padding: EdgeInsets.only(top: 10, bottom: 10),
                color: Colors.black,
                child: Text(
                  'Konversi',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
